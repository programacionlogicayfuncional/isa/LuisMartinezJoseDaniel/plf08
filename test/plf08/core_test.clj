(ns plf08.core-test
  (:require [clojure.test :refer :all]
            [plf08.core :refer :all]
            [plf08.archivos :as arch]
            [plf08.cifradocp :as cp]
            [plf08.cifradotcs :as tcs]))
;;cifrado por cuadrado de polibio tests
(deftest alfabeto-test
  (testing "Prueba de alfabetos"
    (is (map? (cp/alfabeto)))
    (is (map? (cp/alfabeto-2)))))
(deftest cifrar-test
  (testing "Prueba de cifrado de texto"
    (is (= "♜♔♞♕♞♚♜♜" (cp/cifrar "hola")))
    (is (= "♞♖♝♖♞♘♜♚♞♕" (cp/cifrar "mundo")))
    (is (= "♜♛♞♚♞♕♞♝♝♖♝♝♜♖" (cp/cifrar "clojure")))))
(deftest descifrar-test
  (testing "Prueba de descifrado de texto"
    (is (= "hola" (cp/descifrar "♜♔♞♕♞♚♜♜")))
    (is (= "mundo" (cp/descifrar "♞♖♝♖♞♘♜♚♞♕")))
    (is (= "clojure" (cp/descifrar "♜♛♞♚♞♕♞♝♝♖♝♝♜♖")))))

;;Transposición columnar simple tests
(deftest alfabeto-tcs-test
  (testing "Prueba de alfabeto de texto"
    (is (= "canción" (tcs/alfabeto "Canción")))
    (is (= "canción" (tcs/alfabeto "CANCIÓN")))
    (is (= "canción" (tcs/alfabeto "cAnCiÓn")))
    (is (= "clojure" (tcs/alfabeto "CLOJURE")))
    (is (= "clojure" (tcs/alfabeto "CLOJURe")))
    (is (= "clojure" (tcs/alfabeto "cLOjURE")))))
(deftest encriptar-test
  (testing "Prueba de encriptación de texto"
    (is (= "HIT E  SBESYUKL" (tcs/encriptar "canción" "THE SKY IS BLUE")))
    (is (= "TE.F  SH .EI.IAL " (tcs/encriptar "clojure" "THE LIFE IS A ...")))
    (is (= "HilbT fuEs  ilee" (tcs/encriptar "bailando" "THE life is blue")))
    (is (= "hoifecct ciloeln" (tcs/encriptar "master" "chief collection")))
    (is (= "yaYIy  scaro' zum" (tcs/encriptar "botellas" "You say I'm crazy")))))
(deftest desencriptar-test
  (testing "Prueba de desencriptación de texto"
    (is (= "THE SKY IS BLUE" (tcs/desencriptar "canción" "HIT E  SBESYUKL")))
    (is (= "THE LIFE IS A ..." (tcs/desencriptar "clojure" "TE.F  SH .EI.IAL ")))
    (is (= "THE life is blue" (tcs/desencriptar "bailando" "HilbT fuEs  ilee")))
    (is (= "chief collection" (tcs/desencriptar "master" "hoifecct ciloeln")))
    (is (= "You say I'm crazy" (tcs/desencriptar "botellas" "yaYIy  scaro' zum")))))
;;test cargar archivo
(deftest cargar-archivo-test
  (testing "Prueba de carga de archivo"
    (is (string? (arch/cargar-archivo "C:\\hola.txt")))
    (is (string? (arch/cargar-archivo "C:\\kafka.txt")))
    (is (string? (arch/cargar-archivo "C:\\agente.txt")))
    (is (string? (arch/cargar-archivo "C:\\TCS-kafka.txt")))))

(deftest escribir-archivo-test
  (testing "Prueba de escritura de archivo"
    (is (nil? (arch/escribir-archivo "agente.txt" "Esto es solo una prueba de escritura")))
    (is (nil? (arch/escribir-archivo "hola.txt" "En donde el texto es el contenido")))
    (is (nil? (arch/escribir-archivo "kafka.txt" "Y en los test de main se usarán las funciones")))))
;; test main
(deftest main-test
  (testing "Prueba del main de archivo"
    (is (nil? (-main "cifrar" "C:\\agente.txt" "agentecifrado.txt")))
    (is (nil? (-main "cifrar" "C:\\hola.txt" "holacifrado.txt")))
    (is (nil? (-main "cifrar" "C:\\kafka.txt" "kafkacifrado.txt")))
    (is (nil? (-main "descifrar" "C:\\Users\\josed\\Desktop\\plf08\\resources\\holacifrado.txt" "holadescifrado.txt")))
    (is (nil? (-main "descifrar" "C:\\Users\\josed\\Desktop\\plf08\\resources\\agentecifrado.txt" "agentedescifrado.txt")))

    (is (nil? (-main "cifrar" "clojures" "C:\\kafka.txt" "TCS-kafka-encrip.txt")))
    (is (nil? (-main "cifrar" "Botellas" "C:\\frase1.txt" "TCS-frase1-encrip.txt")))
    (is (nil? (-main "cifrar" "minoculares" "C:\\frase2.txt" "TCS-frase2-encrip.txt")))
    (is (nil? (-main "cifrar" "LuisMart" "C:\\frase3.txt" "TCS-frase3-encrip.txt")))
    (is (nil? (-main "cifrar" "abduzcan" "C:\\frase4.txt" "TCS-frase4-encrip.txt")))
    (is (nil? (-main "cifrar" "DanyDany" "C:\\frase5.txt" "TCS-frase5-encrip.txt")))

    (is (nil? (-main "descifrar" "clojures" "C:\\TCS-kafka-encrip.txt" "TCS-kafka-des.txt")))
    (is (nil? (-main "descifrar" "Botellas" "C:\\TCS-frase1-encrip.txt" "TCS-frase1-des.txt")))
    (is (nil? (-main "descifrar" "minoculares" "C:\\TCS-frase2-encrip.txt" "TCS-frase2-des.txt")))
    (is (nil? (-main "descifrar" "LuisMart" "C:\\TCS-frase3-encrip.txt" "TCS-frase3-des.txt")))
    (is (nil? (-main "descifrar" "abduzcan" "C:\\TCS-frase4-encrip.txt" "TCS-frase4-des.txt")))
    (is (nil? (-main "descifrar" "DanyDany" "C:\\TCS-frase5-encrip.txt" "TCS-frase5-des.txt")))))

