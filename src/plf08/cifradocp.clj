(ns plf08.cifradocp
  (:require [clojure.set :as set]))
(defn alfabeto
  []
  (let [f "♜♞♝♛♚♖♘♗♕♔"
        a (fn [s] (map str (repeat 10 s) f))
        b (fn [] (flatten (map a f)))
        c (vec (b))
        d (apply vector "aábcdeéfghiíjklmnñoópqrstuúüvwxyz01234!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~56789")
        e (fn [] (zipmap d c))]
    (e)))
;;alfabeto inverso
(defn alfabeto-2
  []
  (let [f (fn [] (set/map-invert (alfabeto)))]
    (f)))
;;funcion cifrar
(defn cifrar
  [s]
  (apply str (replace (alfabeto) s)))
;;función descifrar
(defn descifrar
  [s]
  (let [f (vec (take-nth 2 (map str s (drop 1 s))))]
    (apply str (replace (alfabeto-2) f))))