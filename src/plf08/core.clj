(ns plf08.core
  (:gen-class)
  (:require [plf08.archivos :as arch]
            [plf08.cifradocp :as cp]
            [plf08.cifradotcs :as tcs]))

;;main
(defn -main
  ([op archivo-lectura archivo-escritura]
   (let [o (apply str op)
         f (fn [] (arch/escribir-archivo (apply str archivo-escritura)
                                         (cp/cifrar (arch/cargar-archivo (apply str archivo-lectura)))))
         g (fn [] (arch/escribir-archivo (apply str archivo-escritura)
                                         (cp/descifrar (arch/cargar-archivo (apply str archivo-lectura)))))
         h (cond (= o "cifrar") (f)
                 (= o "descifrar") (g)
                 :else "No se ha elegido la operacion")]
     h))
  ([op clave archivo-lectura archivo-escritura]
   (let [o (apply str op)
         f (fn [] (arch/escribir-archivo (apply str archivo-escritura)
                                         (tcs/encriptar (apply str clave)
                                                        (arch/cargar-archivo (apply str archivo-lectura)))))
         g (fn [] (arch/escribir-archivo (apply str archivo-escritura)
                                         (tcs/desencriptar (apply str clave)
                                                           (arch/cargar-archivo (apply str archivo-lectura)))))
         h (cond (= o "cifrar") (f)
                 (= o "descifrar") (g)
                 :else "No se ha elegido la operacion")
         i (if (>= (count clave) 8) h
               (println "La longitud minima de la clave debe ser 8"))]
     i)))