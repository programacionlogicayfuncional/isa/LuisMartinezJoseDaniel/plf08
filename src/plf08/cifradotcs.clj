(ns plf08.cifradotcs
  (:require [clojure.string :as str]))

(defn alfabeto
  [s]
  (str/lower-case s))
;; obtener columnas de filas
(defn columnas
  [v]
  (let [a (fn [] (range (apply max (map count v))));;Se obtiene el mayor de los vectores
        b (fn [n] (mapv (fn [vs] (get vs n))
                        (filter (fn [vs] (contains? vs n)) v)))
        c (fn [] (mapv b (a)))]
    (c)))
;;Encriptar un mensaje con clave
(defn encriptar
  [clave mensaje]
  (let [g (fn [clave mensaje] (vec (map vec (partition-all (count (alfabeto clave)) mensaje))))
        f (fn [clave mensaje] (columnas (g clave mensaje)))
        i (fn [clave mensaje] (map vector clave (f clave mensaje)))
        j (fn [clave mensaje] (apply str (flatten (map second (sort-by first (i clave mensaje))))))]
    (j clave mensaje)))
;;Desencriptar un mensaje con clave
(defn desencriptar
  [clave mensaje]
  (let [l (fn [clave mensaje] (vec (map vec (partition-all (count (alfabeto clave)) mensaje))))
        m (fn [clave mensaje] (columnas (l clave mensaje)))
        n (map second (sort-by first (map vector clave (map count (m clave mensaje)))))
        fin (fn [] (reductions + n))
        inicio (fn [] (cons 0 (fin)))
        f (fn [mensaje] (map (partial subvec (apply vector mensaje)) (inicio) (fin)))
        g (fn [clave] (sort-by first (map vector clave (range (count clave)))))
        h (fn [clave mensaje] (map vector (sort clave) (f mensaje) (map second (g clave))))
        i (fn [clave mensaje] (sort-by last (h clave mensaje)))
        j (fn [clave mensaje] (map second (i clave mensaje)))
        k (fn [clave mensaje] (apply str (flatten (columnas (j clave mensaje)))))]
    (k clave mensaje)))
