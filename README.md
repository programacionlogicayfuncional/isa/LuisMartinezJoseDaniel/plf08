# plf08
"Cifrado por transposición"
"Transposición columnar simple"
* Descripción del proyecto:
Este tipo de encriptado, describe la forma en que dado una clave y un mensaje, este se debe encriptar ordenando el mensaje segun sea el numero de caracteres de la clave, luego de ello obtener las columnas de cada letra de la clave, ordenas alfabeticamente la clave y posteriormente tomar cada columna y juntarlo en una sola frase.

1. Lo primero que se ha hecho, agregar archivos .clj, con el fin de dividir en namespaces diferentes para cada cifrado, así también se ha aislado las funciones de lectura y de escritura en otro namespace, y el main. 
2. Luego de ello se ha procedido a crear las funciones para encriptado y desencriptado, teniendo en cuenta, que la clave puede estar en mayusculas y minusculas, por lo que para el alfabeto solo se ha optado por usar clojure.string/lower-case, esto para poder ordenar las letras alfabeticamente. Así mismo se creo una función que permite tener las columnas de manera vertical, dada un numero de vectores en fila. La encriptación se realizo:
- Crear un vector de vectores particionandolos dado el tamaño de la clave
- Obteniendo las columnas
- Asociando la clave con cada columna
- Ordenando alfabeticamente por letra y finalmente juntando todas las columnas en un solo texto
Y para el desencriptado, se ha optado de manera similiar que la encriptación con la diferencia de:
- Dado el mensaje obtener un vector de vectores fila
- Obtener las columnas
- Obtener el tamaño que tendra cada columna de acuerdo a la letra
- Asociar cada letra con las columnas correspondientes de manera ordenada
- Asociar cada letra con su posición original
- Ordenar por porsición original las letras con sus columnas
- Obtener el mensaje original

3. El main se modifico para permitir 3 argumentos con el cuadrado de polibrio y 4 argumentos para la transposición columnar simple.
4. Se han añadido los test para cada namespace probando así que funciona correctamente.

* Forma de ejecutar
Para ejecutarlo bastara con pasar cuatro argumentos para transposición columnar simple y tres argumentos si se requiere hacer un cifrado del cuadrado de polibio.

* Ejemplos de uso o ejecución
Desde la terminal ejecutar:
Cifrado:
lein run "cifrar" "clojures" "C:\\kafka.txt" "TCS-kafka-encrip.txt"
lein run "cifrar" "Botellas" "C:\\frase1.txt" "TCS-frase1-encrip.txt"
Descifrado:
lein run  "descifrar" "clojures" "C:\\TCS-kafka-encrip.txt" "TCS-kafka-des.txt"
lein run "descifrar" "Botellas" "C:\\TCS-frase1-encrip.txt" "TCS-frase1-des.txt"

* Errores, limitantes, situaciones no consideradas o particularidades de tu solución.
Se han hecho varias pruebas, y el programa funciona correctamente

"Cuadrado de polibio"
* Descripción del proyecto:
Para realizar dicho proyecto se ha tenido en cuenta el siguiente cuadro:
	♜	♞	♝	♛	♚	♖	♘	♗	♕	♔
♜	a	á	b	c	d	e	é	f	g	h
♞	i	í	j	k	l	m	n	ñ	o	ó
♝	p	q	r	s	t	u	ú	ü	v	w
♛	x	y	z	0	1	2	3	4	!	"
♚	#	$	%	&	'	(	)	*	+	,
♖	-	.	/	:	;	<	=	>	?	@
♘	[	\	]	^	_	`	{	|	}	~
♗	5	6	7	8	9					
♕										
♔										

Este cuadro se baso, de acuerdo a las especificaciones dadas por el docente acerca del Cuadrado de Polibio.
-Se definio alfabetos mediante un zipmap, para hacer la busqueda de los valores más facilemente
-Se utilizó dos funciones lectura y escritura para los archivos
-Por ultimo el main

Forma de ejecutar:
Para su ejecución simplemente hacer uso de la terminal:
-lein run "opración (cifrado/descifrado)" "la ruta de lecutra.txt" "nombre-escritura.txt"
Ejemplos de ejecución
-lein run "cifrar" "C:\\agente.txt" "agentecifrado.txt"
-lein run "cifrar" "C:\\hola.txt" "holacifrado.txt"
-lein run "descifrar" "C:\\Users\\josed\\Desktop\\plf08\\resources\\agentecifrado.txt" "agentedescifrado.txt"

-Errores:
Hay un error en el descifrado pues existe perdida de datos con caracteres que no existen en el alfabeto, funciona normal con letras minusculas y sin espacios